import { ApolloServer } from "apollo-server"
import "./db.js"
import TypeDefPerson from "./src/typedefs/Person.js"
import ResolversPerson from "./src/resolvers/Person.js"

const server = new ApolloServer({
  typeDefs: [TypeDefPerson],
  resolvers: [ResolversPerson],
})

server.listen(4000).then(({ url }) => {
  console.log(`Server apollo listen at ${url}`)
})
