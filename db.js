import mongoose from "mongoose"

const MONGODB_URI = "mongodb://test:test@localhost:27017/graphql"

mongoose
  .connect(MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connected to mongodb")
  })
  .catch((err) => {
    console.error(err)
  })
