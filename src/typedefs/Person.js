import { gql } from "apollo-server"
// Definiciones de los datos
const typeDefs = gql`
  enum YesNo {
    YES
    NO
  }

  type Address {
    street: String!
    city: String!
  }

  type Person {
    name: String!
    phone: String
    address: Address!
    id: ID!
  }

  type Query {
    personCount: Int!
    allPersons(phone: YesNo): [Person]!
    findPerson(name: String!): Person
  }

  input PersonInput {
    name: String!
    phone: String
    street: String!
    city: String!
  }

  type Mutation {
    addPerson(data: PersonInput): Person
    editNumber(name: String!, phone: String!): Person
    deletePerson(id: ID): String
  }
`

export default typeDefs
