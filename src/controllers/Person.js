import { UserInputError } from "apollo-server"
import Person from "../models/Person.js"

const countAll = async () => {
  try {
    return Person.collection.countDocuments()
  } catch (error) {
    /* throw new UserInputError(error.message, {
      invalidArgs: args,
    }) */
  }
}

const getAll = async (args) => {
  try {
    const persons = !args.phone
      ? await Person.find({})
      : await Person.find({ phone: { $exists: args.phone === "YES" } })
    return persons
  } catch (error) {
    /* throw new UserInputError(error.message, {
      invalidArgs: args,
    }) */
  }
}

const findByName = async (args) => {
  try {
    const { name } = args
    return await Person.findOne({ name })
  } catch (error) {
    /* throw new UserInputError(error.message, {
      invalidArgs: args,
    }) */
  }
}

const create = async (args) => {
  const person = new Person({ ...args.data })
  try {
    return await person.save()
  } catch (error) {
    throw new UserInputError(error.message, {
      invalidArgs: args,
    })
  }
}

const editNumber = async (args) => {
  const person = await Person.findOne({ name: args.name })
  if (!person) return

  person.phone = args.phone
  try {
    return await Person.findByIdAndUpdate(person.id, person, {
      new: true,
    })
  } catch (error) {
    throw new UserInputError(error.message, {
      invalidArgs: args,
    })
  }
}

const deleteOne = async ({ id }) => {
  try {
    await Person.findByIdAndDelete(id)
    return "Delete Success"
  } catch (error) {
    throw new UserInputError(error.message, {
      invalidArgs: args,
    })
  }
}

export { countAll, getAll, findByName, create, editNumber, deleteOne }
