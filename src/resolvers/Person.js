import {
  countAll,
  getAll,
  findByName,
  create,
  editNumber,
  deleteOne,
} from "../controllers/Person.js"

// Resolvers
const resolvers = {
  Query: {
    personCount: async () => await countAll(),
    allPersons: async (_, args) => await getAll(args),
    findPerson: async (_, args) => await findByName(args),
  },
  Mutation: {
    add: async (_, args) => await create(args),
    editNumber: async (_, args) => await editNumber(args),
    deletePerson: async (_, args) => await deleteOne(args),
  },
  Person: {
    address: (root) => {
      return { street: root.street, city: root.city }
    },
  },
}

export default resolvers
